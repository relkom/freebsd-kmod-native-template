# The project was terminated on 06.11.2024.
# DO NOT REPORT ANY ISSUES!

# Kernel module native template ![alt text](https://gitlab.com/4neko/freebsd-kmod-native-template/-/raw/master/logo_600.png?ref_type=heads&inline=true)

A template for the FreeBSD kernel module which is built **nativly** without invoking kmod.mk and C compiler.

A rust code is compiled to obj file and linked with LD linker.

## Usage

In order to build project, the following commands should be executed successfully:
```sh
cargo build --release
./post_build.sh
```

A directory `kmod` in root directory of the crate will be created and kernel modules will be placed there.

In order to clean project, the rust code is cleanned by the `cargo`, the `kmod` dir is cleaned by the `post_clean.sh`.
```sh
cargo clean
./post_clean.sh
```

Cargo does not have an option to call a post-build script because Cargo developers have some problems with... I am not sure what is the problem by the way.

## Optimization

By default the optimization is turned on:
```toml
overflow-checks = true
opt-level = "s"
lto = true
```

Opt-level is set to optimize by size. By default, the kernel module use O2 optimization, so the opt-level can be changed to 2.

Link-time optimisation is also enabled.

Overflow checks are enabled to, but in production it is better to have this option disabled, otherwise it will cause kernel panics.


